#!/usr/bin/bash

#variables
#$1=AUTH, $2=REPO, $3=JENKINS_URL

#GET MANIFEST NAME
MANIFEST = "$(ls ./META-INF/*.json)"
MANIFEST_NAME = "$(basename -- $MANIFEST)"

# Trigger Job from Jenkins
curl -X POST '$3/job/$2/buildWithParameters' -u $1 --data NomManifest="$MANIFEST_NAME"
