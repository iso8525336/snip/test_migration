#!/usr/bin/bash

#variables
#$1=AUTH, $2=REPO, $3=GIT_URL, $4=JENKINS_URL

#check pipeline file exist
[ ! -f ./.pipeline] && echo "compileAndPush()" > ./.pipeline

#set current git url
cp ./META-INF/config.xml .
sed -i 's/GIT_URL/$3/g' config.xml

#check if job already build or not
BUILD = "$(cat ./__init__)"
if [ $BUILD -lt 1]; then
  echo "New Job, build"
  curl -X POST '$4/createItem?name=$2' -u $1 --data-binary @config.xml -H "Content-Type:text/xml"
  #check if success here , then
  echo "1" > __init__
else
  echo "Old Job, update"
  curl -X POST '$4/job/$2/config.xml' -u $1 --data-binary @config.xml -H "Content-Type:text/xml"
fi


